[![pipeline status](https://gitlab.com/stratdes/shared-kernel/badges/master/pipeline.svg)](https://gitlab.com/stratdes/shared-kernel/commits/master)

# Introduction

This intends to be a reusable shared kernel for your hexagonal architectured projects. It includes also stuff for CQRS.
Please, consider this vendor like your own code, so don't consider it infrastructure.

# Contributing

In order to make a contribution, you just need to fork the project and, we you are ready, do a merge request.

## Configuring

You need some software to run composer and tests:

- Docker
- docker-compose

You need to install vendor files, so:

```bash
bin/composer.sh install
```

## Running tests

To run the tests you just need to execute:

```bash
bin/test.sh
```

With coverage:

```bash
bin/test-coverage.sh
```