<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Domain\CQRS\WriteModel\Entity;

use StraTDeS\SharedKernel\Domain\CQRS\WriteModel\Entity\EventSourcedAggregateRoot;
use StraTDeS\SharedKernel\Domain\CQRS\WriteModel\Entity\Snapshot;
use StraTDeS\SharedKernel\Domain\Identity\Id;
use StraTDeS\SharedKernel\Tests\Unit\Domain\DomainEvent\DomainEventStub;
use StraTDeS\SharedKernel\Tests\Unit\Domain\DomainEvent\IdStub;

class EventSourcedAggregateRootStub extends EventSourcedAggregateRoot
{
    private $foo;
    private $bar;

    public function __construct(Id $id, string $foo, string $bar)
    {
        parent::__construct();

        $this->recordThat(
            $event = DomainEventStub::fire(
                IdStub::generate(),
                $id,
                [
                    'foo' => $foo,
                    'bar' => $bar
                ]
            )
        );

        $this->applyDomainEventStub($event);
    }

    public function getFoo()
    {
        return $this->foo;
    }

    public function getBar()
    {
        return $this->bar;
    }

    private function applyDomainEventStub(DomainEventStub $event): void
    {
        $this->id = $event->getEntityId();
        $this->foo = $event->getData()['foo'];
        $this->bar = $event->getData()['bar'];
    }

    protected function applySnapshot(Snapshot $snapshot)
    {
        $this->foo = $snapshot->getData()['foo'];
        $this->bar = $snapshot->getData()['bar'];
    }
}