<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Application\CQRS\ReadModel\ValueObject;

use StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject\ListableQueryResult;
use PHPUnit\Framework\TestCase;

class ListableQueryResultTest extends TestCase
{
    /**
     * @test
     */
    public function checkItReturnsProperValuesOnConstructWithNoNullValues()
    {
        // Arrange
        $listableQueryResult = new ListableQueryResult(
            2,
            10,
            100,
            10,
            [
                'foo' => 'bar'
            ],
            ['attributes.name' => 'asc']
        );

        // Act

        // Assert
        $this->assertEquals(2, $listableQueryResult->getPage());
        $this->assertEquals(10, $listableQueryResult->getPerPage());
        $this->assertEquals(100, $listableQueryResult->getTotalResults());
        $this->assertEquals(10, $listableQueryResult->getTotalPages());
        $this->assertEquals([
            'foo' => 'bar'
        ], $listableQueryResult->getFilters());
        $this->assertEquals(['attributes.name' => 'asc'], $listableQueryResult->getSortBy());
    }

    /**
     * @test
     */
    public function checkItReturnsProperValuesOnConstructWithNullValues()
    {
        // Arrange
        $listableQueryResult = new ListableQueryResult(
            2,
            10,
            100,
            10,
            [
                'foo' => 'bar'
            ],
            []
        );

        // Act

        // Assert
        $this->assertEquals(2, $listableQueryResult->getPage());
        $this->assertEquals(10, $listableQueryResult->getPerPage());
        $this->assertEquals(100, $listableQueryResult->getTotalResults());
        $this->assertEquals(10, $listableQueryResult->getTotalPages());
        $this->assertEquals([
            'foo' => 'bar'
        ], $listableQueryResult->getFilters());
        $this->assertEquals([], $listableQueryResult->getSortBy());
    }
}
