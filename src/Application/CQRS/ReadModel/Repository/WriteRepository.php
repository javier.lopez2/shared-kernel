<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Application\CQRS\ReadModel\Repository;

use StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject\ProjectionModel;

interface WriteRepository
{
    public function save(ProjectionModel $readModel): void;

    public function delete(string $id): void;
}