<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject;

final class ProjectionModel implements \JsonSerializable
{
    private $id;
    private $name;
    private $data;

    private function __construct(array $data, $id, ?string $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->data = $data;
    }

    public static function withData(array $data, $id = null, ?string $name = null)
    {
        return new static($data, $id, $name);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function changeData(array $data): void
    {
        $this->data = $data;
    }

    public function jsonSerialize()
    {
        return $this->getData();
    }
}
