<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Application\CQRS\WriteModel\Service;

use StraTDeS\SharedKernel\Application\CQRS\WriteModel\ValueObject\Command;

interface CommandBus
{
    public function handle(Command $command): void;
}