<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Domain\CQRS\WriteModel\Exception;

use StraTDeS\SharedKernel\Domain\Exception\DomainException;

class EntityNotFoundException extends DomainException
{

}