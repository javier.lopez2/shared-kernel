<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Domain\CQRS\WriteModel\Entity;

use StraTDeS\SharedKernel\Domain\DomainEvent\DomainEvent;
use StraTDeS\SharedKernel\Domain\DomainEvent\EventStream;
use StraTDeS\SharedKernel\Domain\Entity\AggregateRoot;

abstract class EventSourcedAggregateRoot extends AggregateRoot
{
    public function reconstitute(
        EventStream $eventStream,
        Snapshot $snapshot = null
    ): AggregateRoot
    {
        if($snapshot) {
            $this->applySnapshot($snapshot);
        }

        $this->applyEventStream($eventStream);
        $this->resetEventStream();

        return $this;
    }

    protected abstract function applySnapshot(Snapshot $snapshot);

    private function applyEventStream(EventStream $eventStream): void
    {
        /** @var DomainEvent $event */
        foreach ($eventStream->getEvents() as $event) {
            $reflectedEvent = new \ReflectionClass($event);
            $eventName = $reflectedEvent->getShortName();
            $applierName = "apply$eventName";
            $reflectedAggregate = new \ReflectionClass($this);
            $method = $reflectedAggregate->getMethod($applierName);
            $method->setAccessible(true);
            $method->invoke($this, $event);
            $method->setAccessible(false);
        }
    }
}